#!/bin/sh

START_TIME=`date +%T`
set -e
echo "Starting sleep at ${START_TIME} for ${SLEEP} seconds"
sleep ${SLEEP}
END_TIME=`date +%T`
echo "Demo ARG Value"
echo ${ARG_DEMO}
echo "Demo Value"
echo ${DEMO}
echo "Ending sleep at ${END_TIME}"
exit $EXIT_CODE