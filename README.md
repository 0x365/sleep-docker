# sleep
sleep is a simple container to run a sleep timer to perform test.

## How to use this image

Print help:

```bash
docker run --rm us.gcr.io/x365-pipes/tools/sleep --help
```

## Docker image

us.gcr.io/x365-pipes/tools/sleep

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| SLEEP (*)                  |  Time in second to run the sleep, _default:_ 30 | 
| EXIT_CODE (*)                  |  Exit code at end of the sleep execution, _default:_ 0 | 

_(*) = required variable._

Run sleep:

```bash
docker run --it \
  -e SLEEP=15 \
  us.gcr.io/x365-pipes/tools/sleep
```

URLs:

Icon taken from https://pxhere.com/en/photo/1447201