FROM alpine:3.12.0
LABEL maintainer="Alexander Moreno Delgado <alexander.moreno@0x365.com>"

ARG ARG_DEMO

ENV SLEEP 30
ENV EXIT_CODE 0

WORKDIR /app
COPY entrypoint.sh .
COPY demo.sh .

RUN chmod +x /app/*.sh
RUN ./demo.sh

#ENTRYPOINT [ "/bin/sh" ]
ENTRYPOINT [ "/app/entrypoint.sh" ]